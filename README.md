# Oasis Project - Embedded OS (Linux)

This is a template for your project.

## Getting started

1. Open a project in Visual Studio Code (VSC)

2. Open devcontainer in VSC

3.In the integrated terminal, enter:

```bash
source /yocto/poky/oe-init-build-env
```

Now you have a development environment ready. Good luck with the development!